import requests
import logging
logger = logging.getLogger()


class BaseApi:

    __BASE_URL = "https://httpbin.testing-studio.com"

    def send(self,method,url,**kwargs):
        URL = self.__BASE_URL + url
        r = requests.request(method,URL,**kwargs)
        logger.info("url is  " + URL)
        logger.info("response is " + r.text)

        return r