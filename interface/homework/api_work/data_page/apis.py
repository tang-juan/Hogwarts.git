from interface.homework.api_work.data_page.base_api import BaseApi


class Api(BaseApi):

    def get_api(self):
        url = "/get"
        r = self.send("get", url)
        return r

    def post_api(self):
        url = "/post"
        payload = {"level": 1, "name": "xiaohong"}
        r = self.send("post", url, json=payload)
        return r
