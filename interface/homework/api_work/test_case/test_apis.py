from interface.homework.api_work.data_page.apis import Api


class TestApis:

    def setup_class(self):
        self.api = Api()

    def test_get_api(self):
        res = self.api.get_api()
        assert res.status_code == 200


    def test_post_api(self):
        res = self.api.post_api()
        assert res.status_code == 200


