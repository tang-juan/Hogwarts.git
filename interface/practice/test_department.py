import requests
import pytest
class TestDepartment:

    def setup_class(self):
        ID = "ww75920d2ea73356f7"
        SECRET = "iCfHgC5mvOm9ah7tm1merjHr-_DeUlTgdJoKvoinDuk"
        url = f"https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={ID}&corpsecret={SECRET}"
        r = requests.post(url)
        self.access_token = r.json()['access_token']
        print(self.access_token)

    @pytest.mark.parametrize("name",["","1","1"*32,"1"*33])
    def test_create_department(self,name):
        url = f"https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token={self.access_token}"
        data = {
               "name": name,
               "name_en": "RDGZ",
               "parentid": 1,
               "order": 1,
               "id": 3
                }
        res = requests.post(url,json=data)
        print(res.text)
