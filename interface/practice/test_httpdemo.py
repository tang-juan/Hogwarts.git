import requests
import jsonpath
import json
import base64

class TestHttp:

    def test_query(self):
        payload ={"level":1,"name":"tangjuan"}
        url ='https://httpbin.testing-studio.com/get'
        r = requests.get(url,params=payload)
        print(r.text)
        assert r.status_code == 200

    def test_post_form(self):
        payload = {"level": 1, "name": "唐娟"}
        url = 'https://httpbin.testing-studio.com/post'
        r = requests.post(url, data=payload)
        print(r.text)
        assert r.status_code == 200

    def test_post_json(self):
        payload = {"level": 1, "name": "唐娟"}
        url = 'https://httpbin.testing-studio.com/post'
        r = requests.post(url, json=payload)
        print(r.text)
        assert r.status_code == 200

    def test_headers(self):
        url = 'https://httpbin.testing-studio.com/get'
        r = requests.get(url, headers={'h':"header_demo"})
        print(r.json())
        assert r.json()['headers']['H']== 'header_demo'

    def test_jsonpath(self):
        r = requests.get("https://ceshiren.com/categories.json")
        assert jsonpath.jsonpath(r.json(),'$..name')[0] == '开源项目'

    # 通过请求头传递cookie
    def test_cookie1(self):
        header = {'Cookie':'horgwarts=school'}
        r = requests.get('https://httpbin.testing-studio.com/cookies',headers=header)
        print(r.request.headers)

     # 通过关键字参数传递cookie
    def test_cookie2(self):
        Cookie = {'horgwarts':'school','tearcher':'ad'}
        r = requests.get('https://httpbin.testing-studio.com/cookies',cookies=Cookie)
        print(r.request.headers)

    # 对加密响应做解密
    def test_encode(self):
        url = "http://0.0.0.0:9999/keyrank.txt"
        r = requests.get(url)
        res = json.loads(base64.b64decode(r.content))
        print(res)


    def test_proxy_json(self):
        """
        请求头 content-type 为：application/json
        :return:
        """
        url = 'https://httpbin.org/post'
        # 不管是http还是HTTPS请求都会经过 192.168.199.167:8888 代理
        proxy ={
            "http":'http:192.168.199.167:8888',
             "https":'https:192.168.199.167:8888'
        }
        data ={"a":1}
        requests.post(url,json=data,proxies=proxy,verify=False)

    def test_proxy_data(self):
        """
        把传参由json改为  data，请求头则变为 application/x-www-form-urlencoded
        :return:
        """
        url = 'https://httpbin.org/post?a=1'
        # 不管是http还是HTTPS请求都会经过 192.168.199.167:8888 代理
        proxy ={
            "http":'http:192.168.199.167:8888',
             "https":'https:192.168.199.167:8888'
        }
        data ={"a":1}
        requests.post(url,data=data,proxies=proxy,verify=False)

    def test_proxy_file(self):
        """
        把传参改为 file，请求头则变为 multipart/form-data; boundary=8353eac869c871de865fa00a086dea18
        :return:
        """
        url = 'https://httpbin.org/post'
        # 不管是http还是HTTPS请求都会经过 192.168.199.167:8888 代理
        proxy = {
            "http": 'http:192.168.199.167:8888',
            "https": 'https:192.168.199.167:8888'
        }
        requests.post(url, files={'filename': (open("./1.txt","rb"))}, proxies=proxy, verify=False)








