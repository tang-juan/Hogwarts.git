import requests
import json
import base64
import yaml


class ApiRequest:

    env = yaml.load(open("env.yml"))

    def send(self,data:dict):
        data["url"] =str(data["url"]).replace("testing-studio",self.env["testing - studio"][self.env["default"]])
        res = requests.request(data["method"],data["url"],headers=data["headers"])
        if data["encoding"] == "base64":
            return json.loads(base64.b64decode(res.content))
        elif data["encoding"] == "private":
            return None