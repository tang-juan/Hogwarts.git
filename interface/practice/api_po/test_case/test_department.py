import json

from jsonpath import jsonpath

from interface.practice.api_po.page.department import Department

class TestDepartment:

    def setup_class(self):
        # 不要暴露业务逻辑，让测试用例足够简单
        self.department = Department()
        # self.department.get_token()

    def test_create_department(self,get_unique_name):
        r= self.department.create_department(get_unique_name,1)
        assert r.status_code == 200
        assert r.json()["errcode"] == 0
        print(r.json())

    """
    问题：测试数据是否要进行清理？
    1. 没有必要，测试脏数据可以回溯问题
    2. 使用测试环境，不会影响数据
    3. 测试用例少做删除操作，其它测试人员可能在测数据
    """

    def test_update_department(self,get_unique_name):
        r = self.department.create_department(get_unique_name, 1)
        department_id = r.json().get("id")   #  如果使用 r.json()["id"] 因为如果key不存在会报错
        res = self.department.update_department(department_id,name=get_unique_name)
        assert res.status_code == 200
        assert res.json()["errcode"] == 0

    def test_delete_department(self,get_unique_name):
        r = self.department.create_department(get_unique_name, 1)
        department_id = r.json().get("id")
        res = self.department.delete_department(department_id)
        assert res.status_code == 200



    def test_department_smock(self,get_unique_name):

        # 新增部门
        department = self.department.create_department(get_unique_name,1).json()
        departments = self.department.get_department().json()
        # department_id =department['id']
        print(department)
        print(departments)
        # print(json.dumps(departments,indent=2))  # 优化json格式展示
        # print(department_id)

        # assert departments["department"][1]["id"] == department_id   # 写死的断言不灵活

        # 获取所有部门ID，判断
        res_id = jsonpath(departments,"$..id")
        print(res_id)
        # assert department_id in res_id

        #更新部门名称
        # self.department.update_department(department_id)
        # assert self.department.get_department()["department"][1]["name"] == "太阳升"

        # name_res =jsonpath(self.department.get_department(),"$..name")
        # assert "太阳升" in name_res
        #
        # # 删除部门
        # # self.department.delete_department(department_id)
        # print(departments)
        # assert len(self.department.get_department()["department"]) == 1    # 需要重新再次获取



