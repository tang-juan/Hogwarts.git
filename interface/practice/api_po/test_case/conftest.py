import time
import threading
import pytest

@pytest.fixture()
def get_unique_name():
    # 如果只用时间戳，那在多线程情况下会导致时间戳也一样，则可以加上线程名
    name = str(int(time.time())) + threading.current_thread().name

    return name