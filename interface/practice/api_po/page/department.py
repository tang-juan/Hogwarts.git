import requests

from interface.practice.api_po.page.base_api import BaseApi

"""
1、po中的方法，需要把测试数据开放出来，放到参数中
2、注意开发文档中的必填字段
3、写完一个接口要跑一下用例（养成习惯）
"""


class Department(BaseApi):

    def create_department(self,name,parentid,**kwargs):
        """

        :param name: 部门名称
        :param parentid: 父部门ID
        :param kwargs: 其他参数
        :return:
        """
        url = f"/department/create?access_token={self.access_token}"
        # 可以使用**kwargs和update的方法进行测试数据的更新
        print("url is "  + url)
        data = {"name": name,"parentid": parentid}
        data.update(kwargs)  # 更新新增参数到data里
        # res =requests.post(url, json=data)
        res = self.send("post",url,json=data)



        # return  res.json()
        return res    # 要返回res，而不是 res.json()，因为有时候响应内容不是json格式，会导致报错，且res中有状态码等有用信息，使用更灵活

    def update_department(self,department_id,**kwargs):
        url = f"/department/update?access_token={self.access_token}"
        data = {
            "id": department_id
        }
        data.update(kwargs)
        r = self.send("post",url,json=data)
        return r

    def delete_department(self,department_id):
        url = f"/department/delete?access_token={self.access_token}&id={department_id}"
        r = self.send("get",url)
        return r

    def get_department(self,department_id=None):
        #  如果 department_id 填写，获取具体某个部门，不填，获取所有的部门
        if department_id:
            params = {"id":department_id}
        else:
            params = {}
        url = f"/department/list?access_token={self.access_token}"
        r = self.send("get",url,params=params)
        return r