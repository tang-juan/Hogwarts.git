import requests
import logging

logger = logging.getLogger()
"""
1、常量放在类变量中，并且使用全大写，以便修改和使用方便
"""

class BaseApi:
    __BASE_URL = "https://qyapi.weixin.qq.com/cgi-bin"
    __ID = "ww75920d2ea73356f7"
    __SECRET = "iCfHgC5mvOm9ah7tm1merjHr-_DeUlTgdJoKvoinDuk"

    def __init__(self):
        self.access_token = self.get_token()


    def get_token(self):

        url = self.__BASE_URL+f"/gettoken?corpid={self.__ID}&corpsecret={self.__SECRET}"
        r = requests.post(url)
        return r.json()['access_token']

    def send(self, method,url,**kwargs):
        r = requests.request(method,self.__BASE_URL+url,**kwargs)
        logger.info("---url---",self.__BASE_URL+url)
        logger.info("---url content---",r.content)

        return r
