from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver

class BasePage:
    """
    BasePage提供公共方法的封装，即和页面逻辑无关的封装
    比如 driver初始化问题
    """

    # 添加baseurl，可以支持测试用例灵活配置起始页
    # basepage 完全和业务逻辑解耦，
    # 子类会覆盖父类的类变量
    _base_url = ''
    def __init__(self,base_driver=None):
        if base_driver is None:
        # 通过Chrome复用浏览器操作
            chrome_arg = webdriver.ChromeOptions()
            # 加入调试地址
            chrome_arg.debugger_address = '127.0.0.1:9222'
            # 实例化driver对象
            self.driver = webdriver.Chrome(options=chrome_arg)
            # 添加隐式等待，解决问题 no such element: Unable to locate element:
            self.driver.implicitly_wait(5)
            # 打开首页
            # self.driver.get('https://work.weixin.qq.com/wework_admin/frame#index')
            self.driver.get(self._base_url)
        else:
            # 将self.driver 添加一个WebDriver对象注解， 解决类型提示的问题
            # 注解本身没有任何的赋值作用，方便IDE 操作
            self.driver:WebDriver = base_driver

    def find(self,by,elemet=None):
        if elemet is None:
            return self.driver.find_element(*by)
        else:
            return self.driver.find_element(by=by,value=elemet)








