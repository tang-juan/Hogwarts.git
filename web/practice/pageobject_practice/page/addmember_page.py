
from selenium.webdriver.common.by import By

from web.practice.pageobject_practice.page.base_page import BasePage



class AddMemberPage(BasePage):
    _user_name =(By.ID, "username")
    _base_url = ''


    def add_member(self,name):
        """
        添加成员方法
        :return:
        """
        # 导入操作复制在方法内部，解决循环导入问题
        from web.practice.pageobject_practice.page.contact_page import ContactPage
        # 输入姓名。 ID 手机号码
        self.find(*self.user_name).send_keys(name)
        self.find(By.ID, "memberAdd_acctid").send_keys("110")
        self.find(By.ID, "memberAdd_phone").send_keys("13155557777")
        # 点击保存
        self.driver.find_element(By.CSS_SELECTOR, ".js_btn_save").click()
        return ContactPage(self.driver)

