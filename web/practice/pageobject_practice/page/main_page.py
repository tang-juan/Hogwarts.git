from selenium.webdriver.common.by import By

from web.practice.pageobject_practice.page.addmember_page import AddMemberPage
from web.practice.pageobject_practice.page.base_page import BasePage
from web.practice.pageobject_practice.page.contact_page import ContactPage
"""
编码第一步： 构造PO模型，实现设置为空
构造页面相关类和方法
黄色的方块代表一个类
每条线代表这个页面提供的操作
箭头的始端为开始页面
箭头的末端为跳转页面
实现暂时实际为空
"""

class MainPage(BasePage):

    _base_url = 'https://work.weixin.qq.com/wework_admin/frame#index'

    def goto_contact(self):
        """
        跳转到通讯录页面
        :return:
        """

        return ContactPage(self.driver)

    def goto_add_member(self):
        """
        跳转到添加成员页面
        :return:
        """
        self.driver.find_element(By.CSS_SELECTOR,'.ww_indexImg_AddMember').click()
        return AddMemberPage(self.driver)
