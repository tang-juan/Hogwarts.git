import pytest

from web.practice.pageobject_practice.page.main_page import MainPage


class TestAddMember():

    @pytest.mark.parametrize("name", ["皮城女警"])
    def test_add_member(self,name):
        # 1. 跳转到add member页面
        # 2. 添加成员操作， 点击保存跳转到通讯录页面
        # 3. 在通讯录页面获取成员信息，作为断言
        main_page = MainPage()
        assert '皮城女警' in main_page.goto_add_member().add_member(name).get_memberlist()






