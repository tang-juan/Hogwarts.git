from web.practice.set_base import Base

class TestSwitchWindow(Base):

    def test_window(self):
        self.driver.get("http://www.baidu.com")
        self.driver.find_element_by_id("s-top-loginbtn").click()
        # 点击登陆后，查看当前窗口和所有窗口
        print(self.driver.current_window_handle)
        print(self.driver.window_handles)
        self.driver.find_element_by_link_text("立即注册").click()
        # 点击立即注册后，查看当前窗口和所有窗口
        print(self.driver.current_window_handle)
        print(self.driver.window_handles)
        # 切换到注册页窗口后，查看当前窗口和所有窗口
        windows = self.driver.window_handles
        self.driver.switch_to_window(windows[1])
        print(self.driver.current_window_handle)
        print(self.driver.window_handles)

        # 在注册页输入用户名和密码
        self.driver.find_element_by_id("TANGRAM__PSP_4__userName").send_keys("tangjuan")
        self.driver.find_element_by_id("TANGRAM__PSP_4__phone").send_keys("111111")

        #再切回登陆页窗口，进行登陆
        self.driver.switch_to_window(windows[0])
        self.driver.find_element_by_id("TANGRAM__PSP_11__footerULoginBtn").click()
        self.driver.find_element_by_id("TANGRAM__PSP_11__userName").send_keys("tangjuan")
        self.driver.find_element_by_id("TANGRAM__PSP_11__password").send_keys("111111")









