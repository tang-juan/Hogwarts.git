from selenium import webdriver
import os

class Base:
    def setup(self):
        # 通过传不同参数来开启不同的浏览器,在命令行执行  browser=chrome pytest test_switchWindow.py
        # browser = os.getenv("browser")
        # if browser == 'firefox':
        #     self.driver = webdriver.Firefox()
        # elif browser == "chrome":
        #     self.driver = webdriver.Chrome()
        # else:
        #     self.driver = webdriver.Safari()
        self.driver = webdriver.Chrome()
        option = webdriver.ChromeOptions()
        option.add_experimental_option('w3c',False)
        self.driver = webdriver.Chrome(options=option)
        self.driver.maximize_window()
        self.driver.implicitly_wait(5)

    def teardown(self):
        self.driver.quit()