from selenium import webdriver
from selenium.webdriver import TouchActions
from selenium.webdriver.common.keys import Keys
from time import sleep
"""
actionchains 将一系列吧

"""
class TestActionChains():

    def setup(self):
        option = webdriver.ChromeOptions()
        option.add_experimental_option('w3c',False)
        self.driver = webdriver.Chrome(options=option)
        self.driver.maximize_window()

    def teardown(self):
        self.driver.quit()
    # 滚动页面
    def test_scoll(self):
        self.driver.get('http://www.baidu.com')
        ele = self.driver.find_element_by_id('kw')
        ele.send_keys('selenium测试')
        self.driver.find_element_by_id('su').click()
        action = TouchActions(self.driver)
        action.scroll_from_element(ele,0,10000).perform()
        sleep(3)