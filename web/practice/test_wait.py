import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions


class TestWait:

    def setup(self):
        self.driver = webdriver.Chrome()
        self.driver.get("https://home.testing-studio.com/")
        # self.driver.implicitly_wait(3)  #  隐式等待

    def teardown(self):
        self.driver.quit()

    # 直接等待
    def test_wait(self):
        # time.sleep(3)  直接等待
        element = WebDriverWait(self.driver,5).until(expected_conditions.element_to_be_clickable((By.XPATH,'//*[@title="在最近的一年，一月，一周或一天最活跃的话题"]')))
        element.click()
        self.driver.find_element(By.XPATH,'//*[@title="原创精华文章,有100元奖金"]').click()



