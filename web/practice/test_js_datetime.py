import time

from web.practice.set_base import Base

"""
处理时间控件思路：
1、取消控件readonly 属性
2、给value赋值
3、

"""


class TestJsDatetime(Base):

    def test_datetime(self):
        self.driver.get("https://www.12306.cn/index/")
        data_js = "document.getElementById('train_date').removeAttribute('readonly')"
        self.driver.execute_script(data_js)
        update_js = "document.getElementById('train_date').value='2021-06-20'"
        self.driver.execute_script(update_js)
        print("当前时间为")
        print(self.driver.execute_script("return document.getElementById('train_date').value"))
        time.sleep(5)




