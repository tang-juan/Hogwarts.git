from web.practice.set_base import Base


class TestFile(Base):

    def test_file(self):
        self.driver.get("https://image.baidu.com/")
        self.driver.find_element_by_css_selector("#sttb").click()
        self.driver.find_element_by_id("stuurl").send_keys("Users/tangjuan/Downloads/380d36719e85b90ac3a317e67807ccb7.jpeg")

    def test_alert(self):
        """
        1、切换至alert弹框 switch_to.alert()
        2、text:返回alert上的内容
        3、accept() 接受现有弹窗
        4、dismiss()解散现有警告窗

        :return:
        """


