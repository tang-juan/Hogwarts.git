import time

from web.practice.set_base import Base


class TestJs(Base):

    # 使用js操作滑动
    def test_scoll(self):
        self.driver.get("http://www.baidu.com")
        self.driver.find_element_by_id('kw').send_keys("selenium测试")
        ele = self.driver.execute_script("return document.getElementById('su')")  # 使用js定位元素，需要返回值要加return
        ele.click()
        self.driver.execute_script("document.documentElement.scrollTop=10000")
        self.driver.find_element_by_css_selector("#test_case a:nth-child(11)").click()
        # # 多个命令一起执行，使用for,或者使用分号作为参数传入
        # js = ['return document.title','return JSON.stringify(performance.timing)']
        # for code in js:
        #     print(self.driver.execute_script(code))
        print(self.driver.execute_script("return document.title;return JSON.stringify(performance.timing"))

