from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from time import sleep
"""
actionchains 将一系列吧

"""
class TestActionChains():

    def setup(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()

    def teardown(self):
        self.driver.quit()


    # 移动鼠标到一个元素上，悬停操作
    def test_movetoelement(self):
        self.driver.get("http://www.baidu.com")
        ele = self.driver.find_element_by_id("s-usersetting-top")
        action = ActionChains(self.driver)
        action.move_to_element(ele).perform()   # 悬停，移动鼠标到某个元素
        sleep(3)

     # 拖拽元素到从一duo个位置到另置，或者拖动元素距离是
    def test_dragrop(self):
        self.driver.get("https://changba.com/official_login.php")
        drag_element = self.driver.find_element_by_id('nc_1_n1z')
        action = ActionChains(self.driver)
        action.drag_and_drop_by_offset(drag_element,224,0)
        action.perform()

    def test_keys(self):
        self.driver.get("https://changba.com/official_login.php")
        self.driver.find_element_by_css_selector(".back").click()
        self.driver.find_element_by_id('email').click()
        action = ActionChains(self.driver)
        action.send_keys("xiaott008").pause(2)
        action.send_keys(Keys.SPACE)  # 空格
        action.send_keys("hhhh").pause(1)
        action.send_keys(Keys.BACK_SPACE)  # 回车
        action.perform()



