from selenium import webdriver
from selenium.webdriver import ActionChains
from time import sleep
import pytest

class TestLogIn():

    def setup(self):
        option = webdriver.ChromeOptions()
        option.add_experimental_option('w3c', False)
        self.driver = webdriver.Chrome(options=option)
        self.driver.maximize_window()
        self.driver.implicitly_wait(5)

    def teardown(self):
        self.driver.quit()

    @pytest.mark.skip()
    def test_showstopper(self):
        self.driver.get("https://changba.com/")
        action = webdriver.ActionChains(self.driver)
        ele = self.driver.find_element_by_css_selector(".right a:nth-child(2)")
        more_ele = self.driver.find_element_by_css_selector(".top-news .more")
        sleep(3)

    def test_login(self):
        self.driver.get("https://changba.com/official_login.php")
        self.driver.find_element_by_css_selector(".back").click()
        self.driver.find_element_by_id('email').send_keys('xiaott008')
        self.driver.find_element_by_id('pwd').send_keys('tj88342608')
        drag_element = self.driver.find_element_by_id('nc_1_n1z')
        login_btn = self.driver.find_element_by_css_selector('.login-btn')
        action = ActionChains(self.driver)
        action.drag_and_drop_by_offset(drag_element, 224, 0)
        action.click(login_btn)
        action.perform()


if __name__ == '__main__':
    pytest.main(['-v','-s','test_login.py'])



