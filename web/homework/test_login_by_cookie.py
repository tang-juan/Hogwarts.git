from selenium import webdriver
from selenium.webdriver.common.by import By
import yaml


class TestWeiXinLogin:

    def setup(self):
        # chrom_arg = webdriver.ChromeOptions()
        # chrom_arg.debugger_address = '127.0.0.1:9222'
        # self.driver =webdriver.Chrome(options=chrom_arg)
        # self.driver.implicitly_wait(5)
        self.driver =webdriver.Chrome()

    def teardown(self):
        self.driver.quit()



    def test_cookie(self):
        # cookies = self.driver.get_cookies()
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
        with open("./cookie_data.yml") as f:
            cookies = yaml.safe_load(f)
        # cookies = [{'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.d2st', 'path': '/', 'secure': False, 'value': 'a8299855'}, {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.vid', 'path': '/', 'secure': False, 'value': '1688850444124443'}, {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wxpay.vid', 'path': '/', 'secure': False, 'value': '1688850444124443'}, {'domain': '.qq.com', 'expiry': 2147385600, 'httpOnly': False, 'name': 'pgv_pvid', 'path': '/', 'secure': False, 'value': '6476423160'}, {'domain': '.qq.com', 'expiry': 2147385600, 'httpOnly': False, 'name': 'pgv_pvi', 'path': '/', 'secure': False, 'value': '3254204416'}, {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wxpay.corpid', 'path': '/', 'secure': False, 'value': '1970324954461278'}, {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.refid', 'path': '/', 'secure': False, 'value': '372849881563419'}, {'domain': 'work.weixin.qq.com', 'expiry': 1623193924, 'httpOnly': True, 'name': 'ww_rtkey', 'path': '/', 'secure': False, 'value': '7guleee'}, {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.sid', 'path': '/', 'secure': False, 'value': '8fnGwCbWzncrTHAe7cky533R-5Lap005w3lFPnvQrGmoGuoIx0BlioE0FUX7Ipoi'}, {'domain': '.qq.com', 'expiry': 1623248807, 'httpOnly': False, 'name': '_gid', 'path': '/', 'secure': False, 'value': 'GA1.2.1706477756.1622880813'}, {'domain': '.qq.com', 'expiry': 1624110667, 'httpOnly': False, 'name': 'ptui_loginuin', 'path': '/', 'secure': False, 'value': '850230111'}, {'domain': '.work.weixin.qq.com', 'expiry': 1625754409, 'httpOnly': False, 'name': 'wwrtx.i18n_lan', 'path': '/', 'secure': False, 'value': 'zh'}, {'domain': '.qq.com', 'expiry': 1686234407, 'httpOnly': False, 'name': '_ga', 'path': '/', 'secure': False, 'value': 'GA1.2.1458313786.1622880813'}, {'domain': '.qq.com', 'expiry': 1885641883, 'httpOnly': False, 'name': 'tvfe_boss_uuid', 'path': '/', 'secure': False, 'value': '44575de198fa6ca7'}, {'domain': '.work.weixin.qq.com', 'expiry': 1647090870, 'httpOnly': False, 'name': 'wwrtx.c_gdpr', 'path': '/', 'secure': False, 'value': '0'}, {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.vst', 'path': '/', 'secure': False, 'value': 'j27yezISEOBKuxextHAlSWPX0r1wtPqMDIdJDWTgByzt8SPdGibgDo7G06KY17QIAW-oROqcqUxzjrv7NyKfQmPGnNMPevvon_Rr6a_73pEXBzbD3N-3HkG7GJSXiarVYGBhlRyFZiJhr-dB5dgLta7yXEqT0ThIfjjcSFnlRiwokPTXR8zFIu_d6eRk8qSA_y_fJ_0TrgLSn-3UQ-RXDqDDgEQosl-BBoKWdu4MCTWS7MdCWm6RYpZjs88dJsFtwvzHbidU4FuWl225QoksWw'}, {'domain': '.work.weixin.qq.com', 'expiry': 1654488087, 'httpOnly': False, 'name': 'Hm_lvt_9364e629af24cb52acc78b43e8c9f77d', 'path': '/', 'secure': False, 'value': '1622880812,1622952087'}, {'domain': '.qq.com', 'expiry': 1896450235, 'httpOnly': False, 'name': 'pac_uid', 'path': '/', 'secure': False, 'value': '1_850230111'}, {'domain': '.qq.com', 'expiry': 2147385600, 'httpOnly': False, 'name': 'o_cookie', 'path': '/', 'secure': False, 'value': '850230111'}, {'domain': '.qq.com', 'expiry': 2147483646, 'httpOnly': False, 'name': 'ptcz', 'path': '/', 'secure': False, 'value': 'f59068011637cfa305630f7a6376047b0cb036cdb11e588e9f5574ecc8099bce'}, {'domain': '.qq.com', 'expiry': 1623162450, 'httpOnly': False, 'name': '_gat', 'path': '/', 'secure': False, 'value': '1'}, {'domain': '.work.weixin.qq.com', 'httpOnly': False, 'name': 'wwrtx.cs_ind', 'path': '/', 'secure': False, 'value': ''}, {'domain': '.qq.com', 'expiry': 2147483648, 'httpOnly': False, 'name': 'RK', 'path': '/', 'secure': False, 'value': 'pcjsIgrNGV'}, {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.ltype', 'path': '/', 'secure': False, 'value': '1'}, {'domain': '.work.weixin.qq.com', 'httpOnly': True, 'name': 'wwrtx.ref', 'path': '/', 'secure': False, 'value': 'direct'}]
        for cookie in cookies:
            self.driver.add_cookie(cookie)
        # 重新打开带有cookie信息的页面
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
        ele = self.driver.find_element(By.CSS_SELECTOR,'#menu_index span')
        assert '首页' == ele.text
        # print(cookies)