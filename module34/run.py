import unittest
if __name__ == '__main__':
    # 执行方法四：匹配某个目录下以test开头的py文件，然后执行其所有测试用例，
    test_dir = "./test_case"
    # discover可以一次调用多个脚本，start_dir为脚本所在目录，pattern是脚本名称匹配规则
    discover = unittest.defaultTestLoader.discover(start_dir=test_dir,pattern="test*.py")
    unittest.TextTestRunner(verbosity=2).run(discover)

