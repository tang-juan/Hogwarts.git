import pytest
import allure
@allure.feature("登陆模块")  # 对登陆模块标注
class TestLogin:
    @allure.story("登陆成功")
    def test_login(self):
        print("这是登陆 测试用例，登陆成功")
        pass
    @allure.story("登陆失败")
    def test_login_a(self):
        print("这是登陆 测试用例，登陆失败")
        pass
    @allure.story("用户名缺失")
    def test_login_b(self):
        print("用户名缺失")
        pass
    @allure.story("密码缺失")
    def test_login_c(self):
        with allure.step("输入用户名"):   # 对关键步骤进行标记
            print("输入用户名")
        with allure.step("输入密码"):
            print("输入密码")
        with allure.step("点击登陆后失败"):
            assert "1" == 1
            print("登陆失败")
            pass

@allure.feature("注册模块")
class Testregiste:
    @allure.step("注册")
    def test_regist(self):
        print("这是注册")
if __name__ == '__main__':
    pytest.main()
