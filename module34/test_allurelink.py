import allure


@allure.link("http://www.baidu.com",name="百度一下")
def test_link():
    print("测试添加链接")


TEST_CASE_LINK = "http://www.baidu.com"
@allure.severity(allure.severity_level.BLOCKER)  #给用例加上优先级
@allure.testcase(TEST_CASE_LINK,"test_case_title")
def test_with_testcase_link():
    print("这是一条测试用例的链接，链接到测试用例管理里面")

@allure.severity(allure.severity_level.NORMAL)
#在执行时 加上--allure-link-pattern=issue:http://ww.mytesttracker.com/.issue/{}
@allure.issue('140',"这是一个issue")
def test_issue_link():
    print("这是一条问题链接")

#按优先级执行时命令： pytest test_allurelink.py --alluredir=./result/7 --allure-severities normal，多个标识用逗号


