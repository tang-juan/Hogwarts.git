import pytest

# scope = 'function',则会在所有用例执行前执行login函数
# scope = 'class',则会在所有类中执行login函数，类外的用例也会执行
# scope = 'module',则整个模块执行前会执行login函数1次
# scope = 'package'，时、表示整个包都只会执行一次
# scope = 'session',则整个工程项目会执行1次


@pytest.fixture(scope='module',autouse=True)
def login():
    print('登录系统')


def test_01():
    print('测试用例一')

# 在类前添加，则该类的每个用例执行前都会执行login函数
# @pytest.mark.usefixtures('login')
class TestCase1:

    def test_03(self):
        print('测试用例三')

    def test04(self):
        print('测试用例四')

class TestCase2:

    def test_05(self):
        print('测试用例三')

    def test06(self):
        print('测试用例四')

if __name__ == '__main__':
    pytest.main(['-s', 'pytest-demo.py'])
