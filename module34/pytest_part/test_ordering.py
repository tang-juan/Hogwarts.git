
#  使用 pytest -n auto 来进行分布式并发执行用例
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()
import  pytest
from time import sleep

@pytest.mark.run(order=2)
def test_a():
    logger.info("执行测试用例a")

@pytest.mark.run(order=3)
def test_b():
    logger.info("执行测试用例b")


@pytest.mark.run(order=4)
def test_c():
    logger.error("这是错误的日志")

@pytest.mark.first
def test_d():
    sleep(1)