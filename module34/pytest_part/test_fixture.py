import pytest

# 直接传入函数名可以拿到返回值
# 可以传递多个fixture
def test_need_login1(login,connectdb):
    # print(login)
    print("该测试方法需要登陆，直接传入login方法名")


@pytest.mark.usefixtures('login')
def test_need_login2():
    print("该测试方法需要登陆，使用装饰器传入,需要用引号")





def test_no_need_login():
    print("该测试方法不需要需要登陆")