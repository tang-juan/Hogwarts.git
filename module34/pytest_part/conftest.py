import pytest
"""
conftest文件是公共模块，指定目录下全局共享数据，名字不能更改

1、执行：系统执行到参数login时，先在本文件查找是否有这个fixture方法，然后再在conftest.py文件里找是否有

配置注意事项：
1、conftest.py  名称不能更改（重要）
2、conftest.py与运行的case需要在同一个package下，且有_init_文件
3、不需要导入，pytest会自动查找
4、所有同目录测试文件前都会执行conftest.py
5、全局的配置和前期的准备工作都可以放在这里，放在某个包下，这就是全局共享数据的地方
6、优先在离自己最近的父节点搜索conftest.py，如果没有则往上一级寻找，不会再同级的其他的package里找
"""
#fixture 相当于 setup、teardown
#如果autouse=True ，则会对所有用例自动调用
@pytest.fixture(scope='session')
def login():
    print("登陆")
    yield "token"    #  yield 前面的语句相当于setup操作，后面的相当于teardown操作,如果指定了作用域，则指在指定域执行
    print("登出")

@pytest.fixture()
def connectdb():
    print('连接数据库')

# @pytest.fixture(autouse=True)
# def fun1():
#     print('添加自动应用')