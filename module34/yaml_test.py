import sys

import yaml
# 打印字典,字典没有-
# print(yaml.full_load("""
#  a: 1
#
# """))
#打印列表，-代表字典
# print(yaml.load("""
#  - Hesperiidae
#  - Papilionidae
#  - Apatelodidae
#  - Epiplemidae
#  """,Loader=yaml.FullLoader))

#读取文件

# print(yaml.full_load(open("yaml_data.yml")))

# Python转换为yaml，并写入

# with open("demo3.yml", "w+") as f:
#    yaml.dump([{'test': '192.1.1.3'},{ 'dev': '192.3.4.3'}],stream=f)

print(yaml.safe_load(open("./demo3.yml")))