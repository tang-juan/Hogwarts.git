# -*- coding: UTF-8 -*-
import unittest


class Search():
    def search(self):
        print("search")
        return True


class TestSearch(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print("Search1-----setupClass")
        cls.search = Search()

    @classmethod
    def tearDownClass(cls):
        print("Search1------teardownclass")

    def test_search1(self):
        print("test_search1")
        self.assertTrue(self.search.search())

    def test_search2(self):
        print("test_search2")
        self.assertTrue(self.search.search())


class Search2(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print("Search2-----setupClass")

    @classmethod
    def tearDownClass(cls):
        print("Search2------teardownclass")

    def test_add(self):
        self.assertEqual(1, 1, msg="判断1=1")


class Search3(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print("Search3-----setupClass")

    @classmethod
    def tearDownClass(cls):
        print("Search3-------teardownclass")

    def test_add(self):
        self.assertEqual(1, 1, msg='判断1=1')


if __name__ == '__main__':
    # 方法一：执行当前文件所有的测试用例
    unittest.main()
    # 方法二：执行指定的测试用例，将要执行的测试用例加入到一个测试套件，批量执行测试用例
    suite = unittest.TestSuite() # 实例化一个测试套件
    suite.addTest(TestSearch("test_search1"))  # 往测试套件里加测试用例
    suite.addTest(TestSearch("test_search2"))
    unittest.TextTestRunner().run(suite)  # 运行套件suite

    """
    方法三：执行多个测试类,把测试类添加到测试套件里，批量执行测试类
    """
    classsuite1 = unittest.TestLoader().loadTestsFromTestCase(Search2)
    classsuite2 = unittest.TestLoader().loadTestsFromTestCase(Search3)
    suite = unittest.TestSuite([classsuite1,classsuite2])
    unittest.TextTestRunner().run(suite)