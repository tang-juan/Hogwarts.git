import pytest
import yaml


class Test_env:
    @pytest.mark.parametrize("env",yaml.safe_load(open("./env.yml")))
    def test_env(self,env):
        if "test" in env:
            print("这是测试环境，IP是:",env["test"])
        elif "dev" in env:
            print("这是开发环境，IP是:",env["dev"])