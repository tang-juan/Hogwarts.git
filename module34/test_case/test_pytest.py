import pytest
import yaml


def add(a):
    return a +1
# @pytest.mark.parametrize(['a','b'],[
#     (1,2),
#     (2,3),
#     (0,1),
#     (199,201)
# ])

# 使用yaml传入参数
@pytest.mark.parametrize(['a','b'],yaml.safe_load(open("./data.yml"))
)

def test_add1(a,b):
    assert add(a) ==b
@pytest.fixture()
def login():
    print('登陆操作')
    username = 'tom'
    return username

class TestDemo:
    def test_a(self,login):
        print(f"a username is {login}")

    def test_b(self):
        print(f"b username ")

if __name__ == '__main__':
    #第一个参数是要执行的文件，::后边是指定的测试项，第二参数-v是打印详细信息
    pytest.main(['test_pytest.py','-v'])
