# -*- coding: UTF-8 -*-


import yaml
import pytest
from module34.homework.calculate import Calculate
import allure

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

with open("./addition_data.yml") as f1:
    add_data = yaml.safe_load(f1)['add']
    datas = add_data['datas']
    # print(datas)
    add_type = add_data['type']
    # print(add_type)

with open("./division_data.yml") as f2:
    div_data = yaml.safe_load(f2)['div']
    div_datas = div_data['datas']
    div_type = div_data['type']
    print('div_datas-----',div_datas)
    print('div_type------',div_type)


@allure.feature("计算器功能")
class TestCalculate:

    # 实例化计算器

    # def setup_class(cls):
    #     cls.cal = Calculate()
    # # 所有用例执行完后打印
    # def teardown_class(cls):
    #     print("测试计算器结束")
    #
    # # 每条用例开始前打印 开始计算
    # def setup(self):
    #     print("开始计算")
    #
    # def teardown(self):
    #     print("计算结束")

    @allure.title("测试相加_{a}_{b}_{expect}")
    @allure.severity(severity_level=allure.severity_level.CRITICAL)  # 设置critical优先级
    @allure.story("测试加法")   # 设为子模块-加法
    @pytest.mark.parametrize(['a','b','expect'], datas,ids=add_type)
    def test_add(self,a,b,expect,calculator):
        logger.info(f"相加：{a} + {b} = {expect}")
        result = calculator.add(a,b)
        assert result == expect

    @allure.title("测试除法_{a}_{b}_{expect}")  # 测试用例标题
    @allure.severity(severity_level=allure.severity_level.NORMAL)  # 用例优先级
    @allure.story("测试除法")  # 子模块
    @pytest.mark.parametrize(['a','b','expect'],div_datas,ids=div_type)
    def test_div(self,a,b,expect,calculator):
        logger.info(f"除法：{a} / {b} = {expect}")
        result = calculator.div(a, b)
        assert result == expect


