"""
计算器
"""

class Calculate():

    def add(self,a,b):
        """
        加法功能
        :param a: 加数a
        :param b: 加数b
        :return:
        """
        try:
            return round(a + b,2)
        except BaseException:
            return "请输入数字"




    def div(self,a,b):
        """
        除法功能
        :param a: 被除数 a
        :param b: 除数 b
        :return:
        """
        try:
            return round(a / b,2)
        except ZeroDivisionError:
            return "错误"
        except BaseException:
            return "请输入数字"