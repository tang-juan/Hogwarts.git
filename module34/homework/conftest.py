import pytest
from module34.homework.calculate import Calculate

@pytest.fixture(scope='class',autouse=True)
def calculator():
    print("开启一个计算器")
    cal = Calculate()
    yield cal
    print("关闭计算器")

@pytest.fixture(autouse=True)
def prepare():
    print("开始计算")
    yield ' '
    print("结束计算")