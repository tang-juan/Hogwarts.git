from random import randint
class Hero():
    def __init__(self,hp,power,name):  # 初始化英雄的血量、攻击力、姓名
        self.hp = hp
        self.power = power
        self.name = name
        print(f' {name}初始血量是：{hp},攻击力为：{power}')

    def fight(self,enermy_hp,enermy_power):   #  攻击方法
        self.hp = self.hp - enermy_power  # 被敌人攻击，自己剩余血量
        print(f'{self.name}发起攻击，血量剩余{self.hp}')
        self.enermy_final_hp = enermy_hp - self.power # 攻击敌人，敌人剩余血量

if __name__ == '__main__':
    timo = Hero(randint(50,100),randint(10,20),"timo")  # 实例化英雄 timo
    police = Hero(randint(50,100),randint(10,20),"police")  # 实例化敌人 police
    for i in range(3):  #  游戏三个回合
        print(f'第{i+1}回合')
        timo.fight(police.hp,police.power)  # timo先发起攻击
        police.fight(timo.hp,police.power) # police再攻击
    if police.hp < timo.hp:  # 如果police的血量比timo小，则timo获胜
        print(f"恭喜英雄{timo.name}获胜")
    elif police.hp > timo.hp:   # 如果police的血量比timo大，则police获胜
        print(f"很遗憾，敌人{police.name}获胜")
    else:
        print("双方打平")  # 如果相等，则打平
