"""
实战1 原始需求
第一个类Bicycle(自行车)类
run() 方法，打印骑行公里数
第二个类EBicycle继承自Bicycle
volume（电量）属性
fill_charge(vol) 充电方法
run()方法用于骑行,每骑行10km消耗电量1度 ,
当电量消耗尽时调用Bicycle的run方法骑行，
 通过传入的骑行里程数，显示骑行结果
"""



class Bicycle():
    def run(self,miles):
        """
        :param miles: 总的里程数
        :return: 打印骑行数
        """
        print(f'骑行了{miles}公里')


class EBicycle(Bicycle):
    volume = 99
    def fill_charge(self,vol):
        # 在方法内获取变量要用self.
        self.volume += vol
        print(f'充电之后的电量是{self.volume}')

    def run_e(self,miles):
        """
        电动车骑行方法
        :param emile: 电动车骑行里程数
        :return:
        """
        # 通过电量自动获取的里程数
        emile = self.volume * 10
        # 当自动骑行的里程数大于等于总的里程
        if emile >= miles:
            print(f"自动骑行了{emile}公里")
        else:
            print(f"自动骑行了{emile}公里")
            self.run(miles-emile)
            #如果方法重写还想调用父类方法：super().run()

ebike = EBicycle()
ebike.run_e(1000)



