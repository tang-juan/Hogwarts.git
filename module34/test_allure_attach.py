import allure

def test_attach_text():
    allure.attach("这是一个纯文本",attachment_type=allure.attachment_type.TEXT)

def test_html():
    allure.attach("<body>这是body</body>","html 测试块",attachment_type=allure.attachment_type.HTML)

def test_photo():
    allure.attach.file("https://img1.baidu.com/it/u=1280325423,1024589167&fm=26&fmt=auto&gp=0.jpg",name="图片",attachment_type=allure.attachment_type.JPG)