# 作业路径
## Python作业-计算器
### 英雄游戏作业路径： Hogwarts/module 3&4/hero_game.py
### 计算器功能模块：Hogwarts/module 3&4/calculate.py
### 测试计算器功能文件：Hogwarts/module34/test_calculator.py
### 测试后的数据：Hogwarts/module34/result/计算器测试结果
### 生成的报告：Hogwarts/module34/report/计算器测试结果

## web自动化作业-使用cookie登陆企业微信
### 路径：web/homework/test_login_by_cookie.py
## APP自动化作业
### 路径：APP/homework/test_contacts.py
## 接口协议与抓包作业
### 路径：interface/homework

