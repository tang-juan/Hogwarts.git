import yaml
from appium import webdriver
import pytest
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from hamcrest import *


class TestSearch:




    def setup(self):
      desire_cap = {
        "deviceName": "huawei",
        "platformVersion": "10",
        "appActivity": ".view.WelcomeActivityAlias",
        "automationName": "appium",
        "platformName": "Android",
        "autoAcceptAlerts": "true",
        "noReset": "true",  # 是否在测试前后充值相关环境（如首次打开弹框，或者登陆信息）
        "appPackage": "com.xueqiu.android",
        "dontStopAppOnReset": "true",  # 首次启动APP的时候不停止APP（可以调试或者运行的时候提升运行速度）
        "skipDeviceInitiallization": "true",  # 跳过安装、权限等设置（可以调试或者运行的时候提升运行速度）
        "unicodeKeyBoard": "true",  # 设置可以输入中文
        "resetKeyBoard": "true"
      }
      self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desire_cap)
      self.driver.implicitly_wait(10)

    # def teardown(self):
    #   # self.driver.back()
    #   # self.driver.back()
    #   # self.driver.quit()

    def test_search(self):
      self.driver.find_element_by_id("com.xueqiu.android:id/home_search").click()
      self.driver.find_element_by_id("com.xueqiu.android:id/search_input_text").send_keys("阿里巴巴")
      self.driver.find_element_by_id("com.xueqiu.android:id/search_input_text").click()
      self.driver.find_element_by_xpath("//*[@resource-id='com.xueqiu.android:id/name' and @text='阿里巴巴']").click()
      locator = (MobileBy.ID,"com.xueqiu.android:id/current_price")
      current_prince_ele = WebDriverWait(self.driver,10).until(expected_conditions.visibility_of_element_located(locator))
      current_prince = float(current_prince_ele.text)

      assert current_prince > 200

    def test_attribute(self):
      search_ele = self.driver.find_element_by_id("com.xueqiu.android:id/home_search")
      search_enabled = search_ele.is_enabled()
      print(search_ele.is_enabled())
      print(search_ele.text)
      print(search_ele.location)
      print(search_ele.size)
      if search_enabled ==True:
        search_ele.click()
        self.driver.find_element_by_id("com.xueqiu.android:id/search_input_text").send_keys("阿里巴巴")
        self.driver.find_element_by_id("com.xueqiu.android:id/search_input_text").click()
        ele = self.driver.find_element_by_xpath("//*[@resource-id='com.xueqiu.android:id/name' and @text='阿里巴巴']")

        if ele.is_displayed():
          print("搜索成功")
          ele.click()
        else:
          print("搜索失败")

    def test_touchaction(self):
        action =TouchAction(self.driver)
        window_rect = self.driver.get_window_rect()
        width = window_rect['width']
        height = window_rect['height']
        x1 = int(width/2)
        y_start = int(height*4/5)
        y_end = int(height*1/5)
        action.press(x=x1, y=y_start).wait(500).move_to(x=x1, y=y_end).release().perform()  # 也可以传入元素滑动
        # action.press(x=731,y=1853).wait(500).move_to(x=731,y=760).release().perform()

    @pytest.mark.parametrize('searchkey,type,expected_price',yaml.safe_load(open("./search_data.yml")))
    def test_get_current_price(self,searchkey,type,expected_price):
      self.driver.find_element_by_id("com.xueqiu.android:id/home_search").click()
      self.driver.find_element_by_id("com.xueqiu.android:id/search_input_text").send_keys(searchkey)
      self.driver.find_element_by_xpath(f"//*[@resource-id='com.xueqiu.android:id/name']").click()
      # 通过xpath查找兄弟节点
      locator = (MobileBy.XPATH, f"//*[@text='{type}']/../../..//*[@resource-id='com.xueqiu.android:id/current_price']")
      current_prince_ele = WebDriverWait(self.driver, 10).until(
        expected_conditions.visibility_of_element_located(locator))
      current_prince = float(current_prince_ele.text)
      # assert current_prince > 200
    # 使用hamcrest断言
      assert_that(current_prince,close_to(expected_price,expected_price*0.1))
      self.driver.back()

    def test_scroll(self):
        # 滚动查找
      self.driver.find_element_by_android_uiautomator('new UiSelector().text("关注")').click()
      self.driver.find_element_by_android_uiautomator('new UiScrollable(new UiSelector()'
                                                      '.scrollable(true)'
                                                      '.instance(0))'
                                                      '.scrollIntoView(new UiSelector()'
                                                      '.text("雪球私募").instance(0))').click()











if __name__ == '__main__':
  pytest.main()













