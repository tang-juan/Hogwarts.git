from appium import webdriver
import pytest
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class TestSearch:


    def setup(self):
      desire_cap = {
        "deviceName": "huawei",
        "platformVersion": "10",
        "appActivity": ".splash.Welcome",
        "automationName": "appium",
        "platformName": "Android",
        "autoAcceptAlerts": "true",
        "noReset": "true",  # 是否在测试前后充值相关环境（如首次打开弹框，或者登陆信息）
        "appPackage": "com.changba",
        "dontStopAppOnReset": "true",  # 首次启动APP的时候不停止APP（可以调试或者运行的时候提升运行速度）
        "skipDeviceInitiallization": "true",  # 跳过安装、权限等设置（可以调试或者运行的时候提升运行速度）
        "unicodeKeyBoard": "true",  # 设置可以输入中文
        "resetKeyBoard": "true"
      }
      self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desire_cap)
      self.driver.implicitly_wait(10)

    # def teardown(self):
    #   # self.driver.back()
    #   # self.driver.back()
    #   # self.driver.quit()

    def test_toast(self):
      self.driver.find_element(MobileBy.ACCESSIBILITY_ID,"我的").click()
      self.driver.find_element(MobileBy.ACCESSIBILITY_ID,"粉丝").click()
      self.driver.find_element_by_xpath("//android.widget.ListView/android.widget.RelativeLayout[1]/android.widget.LinearLayout").click()
      # print(self.driver.page_source)
      # 通过class属性值定位
      # print(self.driver.find_element(MobileBy.XPATH, "//*[@class='android.widget.Toast']").text)
      # 通过text定位
      print(self.driver.find_element(MobileBy.XPATH, "//*[contains(@text,'你每时每刻')]").text)



if __name__ == '__main__':
  pytest.main()













