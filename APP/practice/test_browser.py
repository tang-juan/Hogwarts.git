from appium import webdriver


class TestBrowser:

    def setup(self):
        desc_caps ={
            'platformName':'android',
            'platformVersion':'10',
            'browserName':'Chrome',
            'noReset':'true',
            "deviceName": "honor v20",
            "chromedriverExecutable":"/usr/local/bin/chromedriver"


        }
        self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desc_caps)
        self.driver.implicitly_wait(10)

    def tearDown(self):
        self.driver.quit()

    def test_browser(self):
        self.driver.get("https://mars.changba.com/tianwen/site/repeat/questionnaire/index/main?qsid=tU3RguR")

