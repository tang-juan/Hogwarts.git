from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from faker import Faker
from hamcrest import *
import yaml

class TestContacts:

    def setup(self):
        with open("./desire_cap.yml") as f:
            desire_cap = yaml.safe_load(f)
        fake = Faker('zh_CN')
        self.name = fake.name()
        self.phone = fake.phone_number()

        self.driver =webdriver.Remote("http://127.0.0.1:4723/wd/hub", desire_cap)
        self.driver.implicitly_wait(5)

    def test_add_contacts(self):
        """
        添加联系人
        :return:
        """

        self.driver.find_element(MobileBy.XPATH,"//*[@text='通讯录']").click()
        self.driver.find_element(MobileBy.XPATH, "//*[@text='添加成员']").click()
        self.driver.find_element(MobileBy.XPATH, "//*[@text='手动输入添加']").click()
        self.driver.find_element(MobileBy.XPATH, "// *[contains( @ text, '姓名')] /../ android.widget.EditText").send_keys(f"{self.name}")
        self.driver.find_element(MobileBy.XPATH, "//*[contains(@text,'手机')]/..//android.widget.EditText").send_keys(f"{self.phone}")
        self.driver.find_element(MobileBy.XPATH, "//*[@text='保存']").click()
        toast = self.driver.find_element(MobileBy.XPATH,"//*[@class='android.widget.Toast']").text
        # 添加成功后返回通讯录获取联系人
        self.driver.back()
        self.driver.find_element(MobileBy.XPATH, "//*[@text='通讯录']").click()
        result = self.driver.find_element(MobileBy.XPATH,f"//*[@text='{self.name}']").text
        # 断言toast 是否添加成功
        assert_that(toast,'添加成功',reason="添加联系人失败")
        # 断言通讯录中的添加的联系人是否与填写一致
        assert_that(result,self.name,reason="添加的联系人姓名与填写的不一致")

    def test_del_contacts(self):
        """
        删除联系人
        :return:
        """

        # 先添加联系人
        self.driver.find_element(MobileBy.XPATH, "//*[@text='通讯录']").click()
        self.driver.find_element(MobileBy.XPATH, "//*[@text='添加成员']").click()
        self.driver.find_element(MobileBy.XPATH, "//*[@text='手动输入添加']").click()
        self.driver.find_element(MobileBy.XPATH,
                                 "// *[contains( @ text, '姓名')] /../ android.widget.EditText").send_keys(f"{self.name}")
        self.driver.find_element(MobileBy.XPATH, "//*[contains(@text,'手机')]/..//android.widget.EditText").send_keys(
            f"{self.phone}")
        self.driver.find_element(MobileBy.XPATH, "//*[@text='保存']").click()

        # 添加联系人后返回通讯录，删除所添加的联系人
        self.driver.back()
        self.driver.back()
        self.driver.find_element(MobileBy.XPATH, f"//*[@text='{self.name}']").click()
        self.driver.find_element(MobileBy.ID,'com.tencent.wework:id/hc9').click() # 点击右上角更多
        self.driver.find_element(MobileBy.XPATH, "//*[@text='编辑成员']").click()
        # 滚动查找删除成员按钮
        self.driver.find_element_by_android_uiautomator('new UiScrollable(new UiSelector()'
                                                        '.scrollable(true)'
                                                        '.instance(0))'
                                                        '.scrollIntoView(new UiSelector()'
                                                        '.text("删除成员").instance(0))').click()
        self.driver.find_element(MobileBy.XPATH, "//*[@text='确定']").click()

        # 断言联系人不在通讯录列表里
        contacts = self.driver.find_elements(MobileBy.XPATH, '//android.view.ViewGroup')
        contacts_list = [name.text for name in contacts]
        assert f'{self.name}' not in contacts_list


















