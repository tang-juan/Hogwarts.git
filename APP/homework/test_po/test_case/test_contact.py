from faker import Faker

from APP.homework.test_po.page.appops import AppOps

import sys
print(sys.path)
class TestContact:

    def setup(self):
        #driver 初始化，app 启动
        self.app = AppOps()
        self.main = self.app.start()
        fake = Faker('zh_CN')
        self.name = fake.name()
        self.phone = fake.phone_number()



    def teardown(self):
        # driver 销毁，app关闭
        self.app.stop()

    def test_add_member(self):
        result =self.main.goto_main_page().goto_contact_page().goto_addmember_page().goto_edit_member_page().addmember(self.name,self.phone).get_tips()
        assert "添加成功" in result


