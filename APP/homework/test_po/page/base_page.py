"""
存放初始化driver，封装find、find_and_click、find_and_send等常用的方法，目的是简化代码，不暴露driver,且在后续优化更改时好维护，以及在添加日志的等可以
统一操作
"""
from appium.webdriver.webdriver import WebDriver
import logging
logger = logging.getLogger()


class BasePage:

    def __init__(self,driver:WebDriver=None):
        self.driver =driver

    def find(self,by,value):
        logger.info("find")
        logger.info(by)
        logger.info(value)
        return self.driver.find_element(by,value)

    def find_and_click(self,by,value):
        logger.info("find_and_click")
        logger.info(by)
        logger.info(value)
        return self.driver.find_element(by, value).click()

    def find_and_send(self,by,value,text):
        logger.info("find_and_send")
        logger.info(by)
        logger.info(value)
        logger.info(text)
        return self.driver.find_element(by, value).send_keys(text)
