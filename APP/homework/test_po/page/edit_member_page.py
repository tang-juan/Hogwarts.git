
from appium.webdriver.common.mobileby import MobileBy

from APP.homework.test_po.page.base_page import BasePage


class EditMemberPage(BasePage):


    def addmember(self,name,phone):
        from APP.homework.test_po.page.addmember_page import AddMemberPage

        self.find(MobileBy.XPATH, "//*[@text='通讯录']").click()
        # 当联系人越来越多时，添加成员按钮不会在初始当前页面，所以需要滑动
        self.driver.find_element_by_android_uiautomator('new UiScrollable(new UiSelector()'
                                                        '.scrollable(true)'
                                                        '.instance(0))'
                                                        '.scrollIntoView(new UiSelector()'
                                                        '.text("添加成员").instance(0))').click()
        # self.find_and_click(MobileBy.XPATH, "//*[@text='添加成员']")
        self.find_and_click(MobileBy.XPATH, "//*[@text='手动输入添加']")
        self.find_and_send(MobileBy.XPATH,
                                 "// *[contains( @ text, '姓名')] /../ android.widget.EditText",f"{name}")
        self.find_and_send(MobileBy.XPATH, "//*[contains(@text,'手机')]/..//android.widget.EditText",f"{phone}")
        self.find_and_click(MobileBy.XPATH, "//*[@text='保存']")


        return AddMemberPage(self.driver)
