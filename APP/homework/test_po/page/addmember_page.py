from appium.webdriver.common.mobileby import MobileBy

from APP.homework.test_po.page.base_page import BasePage


class AddMemberPage(BasePage):


    def goto_edit_member_page(self):
        from APP.homework.test_po.page.edit_member_page import EditMemberPage



        return EditMemberPage(self.driver)

    def get_tips(self):
        return self.driver.find_element(MobileBy.XPATH, "//*[@class='android.widget.Toast']").text
