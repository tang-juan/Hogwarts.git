from APP.homework.test_po.page.base_page import BasePage
from APP.homework.test_po.page.contact_page import ContactPage


class MainPage(BasePage):

    def goto_contact_page(self):
        """
        点击通讯录
        :return:
        """
        return ContactPage(self.driver)


