from APP.homework.test_po.page.addmember_page import AddMemberPage
from APP.homework.test_po.page.base_page import BasePage


class ContactPage(BasePage):

    def goto_addmember_page(self):
        return AddMemberPage(self.driver)