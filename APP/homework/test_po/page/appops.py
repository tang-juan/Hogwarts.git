from appium import webdriver

from APP.homework.test_po.page.base_page import BasePage
from APP.homework.test_po.page.main_page import MainPage


class AppOps(BasePage):


    def start(self):
        if self.driver is None:
            print("driver is None")
            desire_cap = {
                "deviceName": "huawei",
                "platformVersion": "10",
                "appActivity": ".launch.LaunchSplashActivity",
                "automationName": "appium",
                "platformName": "Android",
                "autoAcceptAlerts": "true",
                "noReset": "true",
                "appPackage": "com.tencent.wework",
                "dontStopAppOnReset": "true",
                "skipDeviceInitiallization": "true",
                "unicodeKeyBoard": "true",
                "resetKeyBoard": "true"
            }



            self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desire_cap)
            self.driver.implicitly_wait(5)

            return self
        else:
            print("driver is not None")
            #launch_app()，启动页面，热启动
            self.driver.launch_app()
            return self.driver

    def stop(self):
        self.driver.quit()

    def restart(self):
        pass


    def goto_main_page(self):
        # 入口
        return MainPage(self.driver)

