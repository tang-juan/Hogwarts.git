import yaml
desire_cap = {
            "deviceName": "huawei",
            "platformVersion": "10",
            "appActivity": ".launch.LaunchSplashActivity",
            "automationName": "appium",
            "platformName": "Android",
            "autoAcceptAlerts": "true",
            "noReset": "true",
            "appPackage": "com.tencent.wework",
            "dontStopAppOnReset": "true",
            "skipDeviceInitiallization": "true",
            "unicodeKeyBoard": "true",
            "resetKeyBoard": "true"
        }

with open("./desire_cap.yml",'w+') as f:
    yaml.safe_dump(desire_cap,f)